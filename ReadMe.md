# TODO List

- [ x ] add git to path
- [ x ] create the repository => git init
- [ x ] stage file to git     => git add .\ReadMe.md
- [ x ] commit file changes   => git commit -m "init commit"
- [ x ] show all commits      => git log
- [ x ] checkout by commitID  =>
- [ x ] view the status of uncommited files => git status
- [ x ] adding remote repo    => git remote add origin https://gitlab.com/abdoh-ardi/git-practice.git
- [ x ] pushing to remote and set as upstream AND push all branches and commits => git push -u origin --all
- [ x ] create branch => git branch [branch name]
- [ x ] change working branch => git checkout [branch name]
- [ x ] merge branch => git merge [branch name]
- [ x ] delete merged branch => git branch -d [branch name]